#include <Teclado.h>

Teclado::Teclado(){ }
void Teclado::attach(uint32_t _f0, uint32_t _f1, uint32_t _f2, uint32_t _f3, uint32_t _c0, uint32_t _c1, uint32_t _c2, uint32_t _c3){
    f0=_f0;
    f1=_f1;
    f2=_f2;
    f3=_f3;
    c0=_c0;
    c1=_c1;
    c2=_c2;
    c3=_c3;
    pinMode(f0, OUTPUT);
    pinMode(f1, OUTPUT);
    pinMode(f2, OUTPUT);
    pinMode(f3, OUTPUT);
    pinMode(c0, INPUT_PULLDOWN);
    pinMode(c1, INPUT_PULLDOWN);
    pinMode(c2, INPUT_PULLDOWN);
    pinMode(c3, INPUT_PULLDOWN);
}
int Teclado::getFila(){
    return fila;
}
int Teclado::read(){
    digitalWrite(f3, 0);
    digitalWrite(f0, 1);
    fila=1;
    if(digitalRead(c0)==1){return 1;}
    else if(digitalRead(c1)==1){return 2;}
    else if(digitalRead(c2)==1){return 3;}
    else if(digitalRead(c3)==1){return 10;}
    else {return 16;}

    digitalWrite(f0, 0);
    digitalWrite(f1, 1);
    fila=2;
    if(digitalRead(c0)==1){return 4;}
    else if(digitalRead(c1)==1){return 5;}
    else if(digitalRead(c2)==1){return 6;}
    else if(digitalRead(c3)==1){return 11;}
    else {return 16;}

    digitalWrite(f1, 0);
    digitalWrite(f2, 1);
    fila=3;
    if(digitalRead(c0)==1){return 7;}
    else if(digitalRead(c1)==1){return 8;}
    else if(digitalRead(c2)==1){return 9;}
    else if(digitalRead(c3)==1){return 12;}
    else {return 16;}

    digitalWrite(f2, 0);
    digitalWrite(f3, 1);
    fila=4;
    if(digitalRead(c0)==1){return 14;} //*
    else if(digitalRead(c1)==1){return 0;}
    else if(digitalRead(c2)==1){return 15;} //#
    else if(digitalRead(c3)==1){return 13;}
    else {return 16;}
    

}