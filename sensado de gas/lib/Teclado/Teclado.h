/*
X  X  X  X -- F0
X  X  X  X -- F1
X  X  X  X -- F2
X  X  X  X -- F3
|  |  |  |
|  |  |  | 
C0 C1 C2 C3

1  2  3  4
5  6  7  8
9  10 11 12
13 14 15 16

*/

#ifndef Teclado_h
#define Teclado_h

#include <Arduino.h>

class Teclado{
    public: 
        Teclado();
        void attach(uint32_t, uint32_t, uint32_t, uint32_t, uint32_t, uint32_t, uint32_t, uint32_t);
        int getFila();
        int read();
    private: 
        uint32_t f0;
        uint32_t f1;
        uint32_t f2;
        uint32_t f3;
        uint32_t c0;
        uint32_t c1;
        uint32_t c2;
        uint32_t c3;
        int fila=1;
};

#endif