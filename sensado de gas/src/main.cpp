#include <Arduino.h>
#include <LiquidCrystal.h> 
#include <Teclado.h>

#define setear 0
#define Analisis 1
#define Seguro 2
#define Peligro 3

Teclado teclado;

int gas_max = 20;
int posicion = 0;
int lectura = 0;
int centena = 0;
int decena = 0;
int unidad = 0;

#define rs PB3
#define Ena PB4
#define D4 PB5
#define D5 PB6
#define D6 PB7
#define D7 PB8


LiquidCrystal lcd(rs, Ena, D4, D5, D6, D7);


int Estado = Analisis;

const int sensorPin = PB1;
#define led_r PB12
#define led_v PB13
#define buzzer PB14


void setup() {

pinMode(led_r, OUTPUT);
pinMode(led_v, OUTPUT);
pinMode(buzzer, OUTPUT);
//pinMode(teclado_1, INPUT);
//pinMode(teclado_2, INPUT);

teclado.attach(PA4,PA3,PA2,PA1,PA0,PC15,PC14,PC13);

Serial.begin(9600);
lcd.begin(16,2);

}

void loop() {


    switch (Estado)
    {

        case Setear

            lectura=teclado.read();
            if(posicion==0)
            {
                centena=lectura*100;
                posicion++; 
            }
            else if(posicion == 1)
            {
                decena=lectura*10;
                posicion++;
            }
            else if(posicion == 2)
            {
                unidad=lectura;
                posicion++;
            }
            else
            {
                gas_max=centena+decena+unidad;
                posicion=0;
                Estado=Analisis;
            }

        break;

        case Analisis
           //evaluar porcentaje gas en ambiente

            lectura = teclado.read();
            

            int value = analogRead(sensorPin);
            float millivolts = (value / 1023.0) * 5000;
            float gas = millivolts * 100;
            
                lcd.setCursor(0,1);
                lcd.print(gas);
                lcd.print(" % ");
                delay(50);

            if ( gas => gas_max ) // +20% es peligroso
            {
                Estado = Peligro; 
            
            }
            
            else if (gas < gas_max)       // -20% no es peligroso
            {
                Estado = Seguro;
            }

            else if (lectura == 15)
            {
                Estado = setear;
            }
            


        break;

        case Peligro
            //prender led rojo
            digitalWrite(led_r, HIGH);
            digitalWrite(led_v, LOW);
            
            lcd.setCursor(0,1);
            lcd.print(gas);
            lcd.print(" % ");
            delay(5000);
            
            //enciende buzzer
            digitalWrite(buzzer, HIGH);


            //mensaje en LCD

            lcd.clear();
            lcd.setCursor(0,1);
            lcd.print("PELIGRO CAMBIE");
            lcd.setcursor(0,2)
            lcd.print("DE AMBIENTE");
            delay(50);

            Estado = Analisis;

        break;

        case Seguro

            digitalWrite(led_r, LOW);
            digitalWrite(led_v, HIGH);

            lcd.clear();
            lcd.setCursor(0,1);
            lcd.print("Ambiente seguro");

            Estado = Analisis;


        break;

   
    }

}